import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'app_model.dart';
import 'list_view.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _endpoint = "https://api.rajaongkir.com/starter/";
  final _apikey = "540e72935078bafe128a1c780cb4c2f5";
  final _controller = TextEditingController();
  int _weight = 0;
  int _cost = 0;
  List selected = [0, 0, 0, 0];
  List<Province> _listProvince = new List<Province>();
  List<City> _listCity = new List<City>();

  Future<void> _getListProvince() async {
    Response response = await Dio().get(_endpoint + "province",
        options: Options(
          headers: {"key": _apikey},
        ));

    for (var prov in response.data["rajaongkir"]["results"]) {
      _listProvince.add(new Province(
        id: int.parse(prov["province_id"]),
        name: prov["province"],
      ));
    }
  }

  Future<void> _getListCity() async {
    Response response = await Dio().get(_endpoint + "city",
        options: Options(
          headers: {"key": _apikey},
        ));

    for (var city in response.data["rajaongkir"]["results"]) {
      _listCity.add(new City(
        id: int.parse(city["city_id"]),
        name: city["city_name"],
        idProvince: int.parse(city["province_id"]),
      ));
    }
  }

  void _awaitReturnValueFromListView(BuildContext context, int index) async {
    var result;
    if ((index % 2 == 1) && selected[index - 1] != 0) {
      result = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ListViewerPage(
                    idProvince: selected[index - 1],
                    items: _listCity,
                  )));
      print('Result City : $result');

      setState(() {
        selected[index] = result;
      });
    } else if (index % 2 == 0) {
      // print(_listProvince[0].name);
      result = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ListViewerPage(
                    items: _listProvince,
                  )));

      print('Result Province : $result');

      setState(() {
        selected[index] = result;
      });
    } else
      return;
  }

  void _searchForTheCost() async {
    _weight = int.parse(_controller.text);
    Response response = await Dio().post(
        _endpoint +
            "cost?origin=" +
            selected[1].toString() +
            "&destination=" +
            selected[3].toString() +
            "&weight=" +
            _weight.toString() +
            "&courier=jne",
        options: Options(
          headers: {"key": _apikey},
        ));

    _cost = response.data["rajaongkir"]["results"][0]["costs"][0]["cost"][0]
        ["value"];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 500,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(
                "Provinsi Asal",
                textAlign: TextAlign.left,
              ),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              FutureBuilder(
                future: _getListProvince(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return RaisedButton(
                      child: Text("Choose the first province"),
                      onPressed: () =>
                          _awaitReturnValueFromListView(context, 0),
                    );
                  } else if (snapshot.error) {
                    return Text(snapshot.error);
                  }
                  return CircularProgressIndicator();
                },
              ),
              Text((selected[0] != 0)
                  ? _listProvince[selected[0] - 1].name
                  : "Hai"),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              Text(
                "Kota Asal",
                textAlign: TextAlign.left,
              ),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              FutureBuilder(
                future: _getListCity(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return RaisedButton(
                      child: Text("Choose the first city"),
                      onPressed: () =>
                          _awaitReturnValueFromListView(context, 1),
                    );
                  } else if (snapshot.error) {
                    return Text(snapshot.error);
                  }
                  return CircularProgressIndicator();
                },
              ),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              Text((selected[1] != 0) ? _listCity[selected[1] - 1].name : ""),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              (_listProvince.length == 34)
                  ? RaisedButton(
                      child: Text("Choose the second province"),
                      onPressed: () =>
                          _awaitReturnValueFromListView(context, 2),
                    )
                  : CircularProgressIndicator(),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              Text((selected[2] != 0)
                  ? _listProvince[selected[2] - 1].name
                  : ""),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              (_listCity.length == 501)
                  ? RaisedButton(
                      child: Text("Choose the second city"),
                      onPressed: () =>
                          _awaitReturnValueFromListView(context, 3),
                    )
                  : CircularProgressIndicator(),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              Text((selected[3] != 0)
                  ? _listProvince[selected[3] - 1].name
                  : ""),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              Text("Weight of goods"),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              TextFormField(
                controller: _controller,
              ),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              RaisedButton(
                child: Text("Search for the price"),
                onPressed: () => (selected[0] != 0 &&
                        selected[1] != 0 &&
                        selected[2] != 0 &&
                        selected[3] != 0 &&
                        _weight != 0)
                    ? _searchForTheCost()
                    : null,
              ),
              Padding(
                padding: EdgeInsets.only(top: 9.0),
              ),
              Text((_cost != 0) ? _cost.toString() : ""),
            ],
          ),
        ),
      ),
    );
  }
}
