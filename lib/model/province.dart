class Province {
  ProvinceRajaongkir rajaongkir;

  Province({this.rajaongkir});

  Province.fromJson(Map<String, dynamic> json) {
    rajaongkir = json['rajaongkir'] != null
        ? new ProvinceRajaongkir.fromJson(json['rajaongkir'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.rajaongkir != null) {
      data['rajaongkir'] = this.rajaongkir.toJson();
    }
    return data;
  }
}

class ProvinceRajaongkir {
  List<String> query;
  ProvinceStatus status;
  List<ProvinceData> results;

  ProvinceRajaongkir({this.query, this.status, this.results});

  ProvinceRajaongkir.fromJson(Map<String, dynamic> json) {
    if (json['query'] != null) query = json['query'].cast<String>();
    status =
        json['status'] != null ? new ProvinceStatus.fromJson(json['status']) : null;
    if (json['results'] != null) {
      results = new List<ProvinceData>();
      json['results'].forEach((v) {
        results.add(new ProvinceData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (query != null) data['query'] = query;
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProvinceStatus {
  int code;
  String description;

  ProvinceStatus({this.code, this.description});

  ProvinceStatus.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['description'] = this.description;
    return data;
  }
}

class ProvinceData {
  String provinceId;
  String name;

  ProvinceData({this.provinceId, this.name});

  ProvinceData.fromJson(Map<String, dynamic> json) {
    provinceId = json['province_id'];
    name = json['province'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['province_id'] = this.provinceId;
    data['province'] = this.name;
    return data;
  }
}
