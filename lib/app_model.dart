abstract class ListItem {}

class Province implements ListItem{
  int id;
  String name;

  Province({this.id, this.name});
}

class City implements ListItem{
  int id;
  String name;
  int idProvince;

  City({this.id, this.name, this.idProvince});
}