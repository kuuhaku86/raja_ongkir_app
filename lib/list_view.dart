import 'package:flutter/material.dart';
import 'package:raja_ongkir_app/app_model.dart';

class ListViewerPage extends StatefulWidget {
  final List<ListItem> items;
  final int idProvince;

  ListViewerPage({this.idProvince, this.items});

  @override
  _ListViewerPageState createState() => _ListViewerPageState();
}

class _ListViewerPageState extends State<ListViewerPage> {
  Widget listProvinceBuilder(listItem) {
    return ListView.builder(
      itemCount: listItem.length,
      itemBuilder: (context, index) {
        final item = listItem[index];
        if (item is Province) {
          return ListTile(
            title: Text(item.name),
            onTap: () => _sendDataBack(context, item.id),
          );
        } else if (item is City && item.idProvince == widget.idProvince) {
          return ListTile(
            title: Text(item.name),
            onTap: () => _sendDataBack(context, item.id),
          );
        } else
          return Container(
            color: Colors.amberAccent,
            child: Text("Error"),
          );
      },
    );
  }

  void _sendDataBack(BuildContext context, int id) {
    Navigator.pop(context, id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "List City or Province",
      home: Scaffold(
        body: listProvinceBuilder(widget.items),
      ),
    );
  }
}
