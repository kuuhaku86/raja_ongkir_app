import 'package:dio/dio.dart';
import 'package:raja_ongkir_app/app_model.dart';

final _endpoint = "https://api.rajaongkir.com/starter/";
List<Province> listProvince = new List<Province>();
void getListProvince() async {
    Response response = await Dio().get(
        _endpoint + "city",
        options: Options(
          headers: {"key" : "540e72935078bafe128a1c780cb4c2f5"},
          )
        );
      for(var prov in response.data["rajaongkir"]["results"]){
        listProvince.add(
          new Province(
            id: prov["id"], 
            name: prov["name"]
          )
        );
        // print(prov);
      }
      print(listProvince.length);
  }

main(List<String> args) {
  getListProvince();
  print(listProvince);
}