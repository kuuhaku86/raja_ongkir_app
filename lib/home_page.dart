import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:raja_ongkir_app/model/city.dart';
import 'package:raja_ongkir_app/model/cost.dart';
import 'package:raja_ongkir_app/model/province.dart';
import 'package:toast/toast.dart';

class HomePage extends StatefulWidget {
  final String title;

  const HomePage({Key key, @required this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _controller = TextEditingController();
  int _weight = 0;
  int _cost = 0;
  final _selectedId = <String, dynamic>{};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text('Asal Kota',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontSize: 30, fontWeight: FontWeight.w600)),
                SizedBox(height: 16),
                RaisedButton(
                    onPressed: () async {
                      _selectedId['prov-1'] = await Navigator.push(context,
                          MaterialPageRoute(builder: (_) => ListPage()));
                    },
                    child: Text("Pilih Provinsi")),
                SizedBox(height: 8),
                Text(
                    "Provinsi : ${_selectedId['prov-1'] != null ? (_selectedId['prov-1'] as ProvinceData).name : ''}"),
                SizedBox(height: 16),
                RaisedButton(
                    onPressed: () async =>
                        _selectedId['city-1'] = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => ListPage(
                                      isProvince: false,
                                      provinceId: (_selectedId['prov-1']
                                              as ProvinceData)
                                          .provinceId,
                                    ))),
                    child: Text("Pilih Kota")),
                SizedBox(height: 8),
                Text(
                    "Kota : ${_selectedId['city-1'] != null ? (_selectedId['city-1'] as CityData).cityName : ''}"),
                SizedBox(height: 16),
                Text('Tujuan Kota',
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontSize: 30, fontWeight: FontWeight.w600)),
                SizedBox(height: 16),
                RaisedButton(
                    onPressed: () async => _selectedId['prov-2'] =
                        await Navigator.push(context,
                            MaterialPageRoute(builder: (_) => ListPage())),
                    child: Text("Pilih Provinsi")),
                SizedBox(height: 8),
                Text(
                    "Provinsi : ${_selectedId['prov-2'] != null ? (_selectedId['prov-2'] as ProvinceData).name : ''}"),
                SizedBox(height: 16),
                RaisedButton(
                    onPressed: () async => _selectedId['city-2'] =
                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => ListPage(
                                    isProvince: false,
                                    provinceId:
                                        (_selectedId['prov-2'] as ProvinceData)
                                            .provinceId))),
                    child: Text("Pilih Kota")),
                SizedBox(height: 8),
                Text(
                    "Kota : ${_selectedId['city-2'] != null ? (_selectedId['city-2'] as CityData).cityName : ''}"),
                SizedBox(height: 40),
                TextField(
                  controller: _controller,
                  decoration: InputDecoration(hintText: 'Berat'),
                  keyboardType: TextInputType.number,
                ),
                SizedBox(height: 8),
                RaisedButton(
                    onPressed: () async {
                      if (_selectedId['prov-1'] == null) {
                        Toast.show('Provinsi asal belum terpilih !', context);
                        return;
                      }

                      if (_selectedId['city-1'] == null) {
                        Toast.show('Kota asal belum terpilih !', context);
                        return;
                      }

                      if (_selectedId['prov-2'] == null) {
                        Toast.show('Provinsi tujuan belum terpilih !', context);
                        return;
                      }

                      if (_selectedId['city-2'] == null) {
                        Toast.show('kota tujuan belum terpilih !', context);
                        return;
                      }

                      if (_controller.text.isEmpty) {
                        Toast.show('Mohon isi berat !', context);
                        return;
                      }

                      print(
                          "origin : ${(_selectedId['city-1'] as CityData).cityId}");
                      print(
                          "destination : ${(_selectedId['city-2'] as CityData).cityId}");
                      print("berat : ${_controller.text}");

                      final response = await Dio().post("$_endpoint/cost",
                          data: {
                            "origin":
                                (_selectedId['city-1'] as CityData).cityId,
                            "destination":
                                (_selectedId['city-2'] as CityData).cityId,
                            "weight": _controller.text,
                            "courier": "jne"
                          },
                          options: Options(headers: {'key': _apikey}));
                      final costResponse = CostResponse.fromJson(response.data);
                      costResponse.rajaongkir.results.forEach((v) {
                        print("${v.name}");
                        v.costs.forEach((c) {
                          print('${c.service} - ${c.description}');
                          c.cost.forEach((p) => print("${p.etd} - ${p.value}"));
                        });
                      });
                    },
                    child: Text("OK"))
              ],
            ),
          ),
        ));
  }
}

class ListPage extends StatefulWidget {
  final bool isProvince;
  final provinceId;

  const ListPage({Key key, this.isProvince = true, this.provinceId})
      : super(key: key);

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  var data = List();

  @override
  void initState() {
    if (widget.isProvince)
      _getProvince()
          .then((v) => setState(() => data.addAll(v.rajaongkir.results)));
    else
      _getCity(provinceId: widget.provinceId)
          .then((v) => setState(() => data.addAll(v.rajaongkir.results)));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: (AppBar(
            title: Text('Daftar ${widget.isProvince ? 'Provinsi' : 'Kota'}'))),
        body: Visibility(
          visible: data.length >= 1,
          child: ListView.builder(
            itemCount: data.length,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) =>
                _item(data[index]),
          ),
        ));
  }

  Widget _item(dynamic data) {
    String title = widget.isProvince
        ? (data as ProvinceData).name
        : (data as CityData).cityName;
    return GestureDetector(
      onTap: () => Navigator.pop(context,
          widget.isProvince ? (data as ProvinceData) : (data as CityData)),
      child: Material(
        color: Colors.white,
        elevation: 10,
        child: Container(
          padding: EdgeInsets.all(16),
          child: Text(title),
        ),
      ),
    );
  }

  Future<Province> _getProvince() async {
    final response = await Dio().get('$_endpoint/province',
        options: Options(headers: {"key": _apikey}));
    return Province.fromJson(response.data);
  }

  Future<City> _getCity({@required String provinceId}) async {
    final response = await Dio().get('$_endpoint/city?province=$provinceId',
        options: Options(headers: {"key": _apikey}));
    return City.fromJson(response.data);
  }
}

final _endpoint = "https://api.rajaongkir.com/starter/";
final _apikey = "540e72935078bafe128a1c780cb4c2f5";
